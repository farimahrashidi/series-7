package sbu.cs;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ClientClass {

    private static final int portNumber = 8080;
    private static final String ip = "localhost";
    private static byte[] data;
    private static DataOutputStream writer;
    private static Socket socket;


    public ClientClass() throws IOException
    {
        socket = new Socket(ip, portNumber);
        writer = new DataOutputStream(socket.getOutputStream());
    }

    public static void readData(String fileName) throws IOException
    {
        data = Files.readAllBytes(Paths.get(fileName));
    }

    ////////////////////////////////
    public static void writeData(String fileName) throws IOException
    {
        writer.writeUTF(fileName);
        writer.writeInt(data.length);
        writer.write(data);
    }

    public static void closeAll() throws IOException
    {
        socket.close();
        writer.close();
    }
}
