package sbu.cs;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerClass {

    private static final int portNumber = 8080;
    private static Socket socket;
    private static ServerSocket server;
    private static DataInputStream reader;
    private static String fileName;
    private static byte[] data;


    public ServerClass() throws IOException
    {
        server = new ServerSocket(portNumber);
    }

    public static void waitForClient() throws IOException
    {
        socket = server.accept();
        reader = new DataInputStream(socket.getInputStream());
    }

    public static void read() throws IOException
    {
        fileName = reader.readUTF();
        int len = reader.readInt();

        data = new byte[len];

        reader.readFully(data);
    }

    ////////////////////////////////
    public static void createFile(String directory) throws IOException
    {
        FileOutputStream writer = new FileOutputStream(directory + "/" + fileName);
        writer.write(data);
    }

    public static void closeAll() throws IOException
    {
        server.close();
        socket.close();
        reader.close();
    }
}
